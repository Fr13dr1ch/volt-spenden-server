var jsdom = require('jsdom');
const { JSDOM } = jsdom;
const { window } = new JSDOM();
const { document } = (new JSDOM('')).window;
global.document = document;

var $ = jQuery = require('jquery')(window);

var cacheArrayData = [];
var counter = 0;
var run = false;
var cacheEndData;

function loadData(){
  var enddata = [];
  let id = "1fp9Sf3-iSH1tx4sg4NitFXn6hbS2P_-UZcFyrEBgRcQ";
  $.get("https://spreadsheets.google.com/feeds/cells/"+id+"/1/public/full?alt=json").done(function(data) {
    loadCitys(data.feed.entry);
  });
}

function loadCitys(data) {
  console.log("Load Citys");
  let cityLength = data.length/3;
  //citys =
  for(let i=0; i<data.length; i=i+3){
    let cityName = data[i].content.$t;
    let cityID = data[i+1].content.$t;
    let spendenziel = data[i+2].content.$t;
    let endcity = new City(cityName, cityID, spendenziel);
    postData('https://api.spendino.de/admanager/projects/info', {
      data: {
        project: cityID
      }
    }).then(function(data) {
      endcity.donators = data.donators;
      endcity.amount = data.amount;
      buildElem(endcity, cityLength);
    }.bind(endcity));
  }
  //console.log(JSON.parse(enddata));
}





class City {
  constructor(name, id, spendenziel = 2000, donators = 0, amount = 0) {
    this.name = name;
    this.id = id;
    this.spendenziel = spendenziel;
    this.donators = donators;
    this.amount = amount;
  }

  toJSON(){
    return {
      name: this.name,
      id: this.id,
      spendenziel: this.spendenziel,
      donators: this.donators,
      amount: this.amount
    }
  }
}

function buildElem(city, cityLength) {
  counter++;
  cacheArrayData.push(JSON.stringify(city.toJSON()));
  if(counter == cityLength){
    cacheEndData = "[";
    for(let i=0; i<cacheArrayData.length; i++){
      cacheEndData = cacheEndData + cacheArrayData[i] + ",";
    }
    cacheEndData = cacheEndData.substring(0, cacheEndData.length-1);
    cacheEndData = cacheEndData + "]";
    counter = 0;
    run = false;
    cacheArrayData = [];
  }
}



function postData(url = '', data = {}) {
  return $.post(url, data).done(function(data) {
    return data;
  });
}

function numberWithPoints(x) {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".");
}





var http = require('http');

function createHTTP(){


}

createHTTP();

http.createServer(function (req, res) {
  if(!run){
    loadData();
    run = true;
  }

  try{
    res.writeHead(200, {'Content-Type': 'application/json; charset=utf-8'});
    res.write(cacheEndData);
    res.end();

  }catch(e){
    createHTTP();
  }

}).listen(8080);
